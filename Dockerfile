FROM php:7.3-alpine
MAINTAINER Evgeny Gushchin <latenal@gmail.com>

RUN apk update && apk upgrade && apk add --no-cache autoconf build-base bash git openssh icu-dev freetype freetype-dev libpng libpng-dev libjpeg-turbo zlib-dev zlib sqlite sqlite-dev bzip2 bzip2-dev curl curl-dev openssl libmcrypt libmcrypt-dev gmp gmp-dev libzip-dev findutils libgcc libstdc++ libx11 glib libxrender libxext libintl ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family sshpass
RUN mkdir -p ~/.ssh
RUN echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
RUN docker-php-ext-install intl zip bz2 curl exif ftp pdo sockets gd gmp hash pdo_mysql
RUN pecl install redis && docker-php-ext-enable redis
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php --install-dir=/bin --filename=composer && php -r "unlink('composer-setup.php');"
RUN pear install PHP_CodeSniffer
RUN pecl install pcov
RUN docker-php-ext-enable pcov
RUN set -ex \
    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
    && pecl install imagick-3.4.3 \
    && docker-php-ext-enable imagick \
    && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
    && apk del .phpize-deps
COPY --from=madnight/alpine-wkhtmltopdf-builder:0.12.5-alpine3.10-606718795 /bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
